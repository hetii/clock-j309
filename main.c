  #include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>         //sprintf
//PD 3,4,5,6 digits
//PB 7 - B
//PB 6 - F
//PB 5 - A
//PB 4 - G
//PB 3 - C
//PB 2 - D
//PB 1 - H
//PB 0 - E
#define DAT_L (PORTD &= ~_BV(PD0))
#define DAT_H (PORTD |= _BV(PD0))
#define CLK_L (PORTD &= ~_BV(PD1))
#define CLK_H (PORTD |= _BV(PD1))

char diodes[60][2] = {
{3, 5}, {7, 5}, {6, 5}, {5, 5}, {5, 4}, {6, 4}, 
{7, 4}, {3, 4}, {4, 4}, {0, 4}, {1, 4}, {2, 4}, 
{2, 3}, {1, 3}, {0, 3}, {4, 3}, {7, 3}, {6, 3}, 
{5, 3}, {5, 2}, {6, 2}, {7, 2}, {4, 2}, {3, 2}, 
{0, 2}, {1, 2}, {2, 2}, {2, 1}, {1, 1}, {0, 1}, 
{3, 1}, {7, 1}, {6, 1}, {5, 1}, {5, 0}, {6, 0}, 
{7, 0}, {3, 0}, {4, 0}, {0, 0}, {1, 0}, {2, 0}, 
{2, 7}, {1, 7}, {0, 7}, {4, 7}, {7, 7}, {6, 7}, 
{5, 7}, {5, 6}, {6, 6}, {7, 6}, {4, 6}, {3, 6}, 
{0, 6}, {1, 6}, {2, 6}, {2, 5}, {1, 5}, {0, 5}};

void sendbyte(char digit){
  unsigned char k;
  for (k=0; k<8; k++){
    if (digit & 0x80){
      DAT_H; // defined in header to set data pin high
      CLK_H; // defined in header to set shift pin high
      CLK_L; // defined in header to set shift pin low
    } else {
      DAT_L; // defined in header to set data pin low
      CLK_H;
      CLK_L;
    }
    digit = digit << 1; // shift the bits in your byte to the left 1 position
  } 
}

void test_shift_register(void){
  unsigned char i;
  i=0;
  while(1){
    sendbyte(~i);
    i = i*2;
   _delay_ms(430); 
    if (i == 256||i==0) i=1;
  }
}

void set_diode(char count){
  unsigned char i, a, b;
  a = 0xFF;
  b = 0xFF;

  for (i = 0; i < count; ++i)
  {
    a &= ~(1<<diodes[i][0]);
    b &= ~(1<<diodes[i][1]);
  }
  PORTB = a;
  sendbyte(b);
}
//a &= ~(1<<6) - '0b10000001'
//a |= (1<<6)  - '0b11000001'
int main(int argc, char const *argv[])
{
  int i;
  DDRB  = 0xFF; //P1 in 89c2051
  DDRD  = 0xFF; //P3 in 89c2051
  PORTB = 0xFF; //All segments off
  PORTD = 0xFF; //All digits off
  
  //test_shift_register();
  //PORTB=0b11111110;
  //sendbyte(0);
  while(1){
  set_diode(15);
}
/*  
PORTB=0;
while(1){
  for (i = 0; i <= 59; ++i){
     //PORTB &= ~(1<<diodes[i][0]);
     sendbyte(0xFF & ~(1<<diodes[i][1]));
     _delay_us(100);
     //PORTB = 0xFF;
  } 
}
  */
  return 0;
}
